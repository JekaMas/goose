package goose

import (
	"errors"

	ini "github.com/rakyll/goini"
)

type iniParser Config

func newIniParser(config Config) iniParser {
	return iniParser(config)
}

func (this iniParser) ConfigParse() (Config, error) {
	var ok bool

	dict, err := ini.Load(this.DBConfigFilePath)
	if err != nil {
		return Config{}, err
	}

	if this.DBType == "" {
		if this.DBType, ok = dict.GetString("", this.DBTypeConfigKey); ok != true {
			return Config{}, errors.New(emptyParameterErrorMessage)
		}
	}

	if this.DSN, ok = dict.GetString("", this.DSNConfigKey); ok != true {
		return Config{}, errors.New(emptyParameterErrorMessage)
	}

	if importValue, ok := dict.GetString("", this.ImportConfigKey); ok {
		this.Import = importValue
	}

	if dialect, ok := dict.GetString("", this.DialectConfigKey); ok {
		this.Dialect = dialect
	}

	return Config(this), nil
}
