package goose

import "github.com/kylelemons/go-gypsy/yaml"

type yamlParser Config

func newYamlParser(config Config) yamlParser {
	return yamlParser(config)
}

func (this yamlParser) ConfigParse() (Config, error) {
	f, err := yaml.ReadFile(this.DBConfigFilePath)
	if err != nil {
		return Config{}, err
	}

	if this.DBType == "" {
		if this.DBType, err = f.Get(yamlKey(this.Env, this.DBTypeConfigKey)); err != nil {
			return Config{}, err
		}
	}

	if this.DSN, err = f.Get(yamlKey(this.Env, this.DSNConfigKey)); err != nil {
		return Config{}, err
	}

	if importValue, err := f.Get(yamlKey(this.Env, this.ImportConfigKey)); err == nil {
		this.Import = importValue
	}

	if dialect, err := f.Get(yamlKey(this.Env, this.DialectConfigKey)); err == nil {
		this.Dialect = dialect
	}

	return Config(this), nil
}
