package goose

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"path"

	"github.com/lib/pq"
)

// DBDriver encapsulates the info needed to work with
// a specific database driver
type DBDriver struct {
	Name    string
	OpenStr string
	Import  string
	Dialect SqlDialect
}

type DBConf struct {
	MigrationsDir string
	Env           string
	Driver        DBDriver
	PgSchema      string
}

var dbDrivers = map[string]DBDriver{
	"mysql": DBDriver{
		Name:    MysqlDialectName,
		Import:  "github.com/go-sql-driver/mysql",
		Dialect: &MySqlDialect{},
	},
	"mymysql": DBDriver{
		Name:    MyMysqlDialectName,
		Import:  "github.com/ziutek/mymysql/godrv",
		Dialect: &MySqlDialect{},
	},
	"postgres": DBDriver{
		Name:    PostgresDialectName,
		Import:  "github.com/lib/pq",
		Dialect: &PostgresDialect{},
	},
	"sqlite3": DBDriver{
		Name:    Sqlite3DialectName,
		Import:  "github.com/mattn/go-sqlite3",
		Dialect: &Sqlite3Dialect{},
	},
}

const (
	defaultDBConfFilePath   = "."
	defaultDBConfFile       = "dbconf.yml"
	defaultDBMigrationsPath = "migrations"

	iniExtension  = "ini"
	yamlExtension = "yml"
	yamlKeyFormat = "%s.%s"

	driverParameter  = "driver"
	openParameter    = "open"
	importParameter  = "import"
	dialectParameter = "dialect"

	postgresDriver = "postgres"

	emptyParameterErrorMessage = "Paratemer is empty"
)

// NewDBConf extracts configuration details from the given file
func NewDBConf(config Config) (*DBConf, error) {
	setConfigDefaults(&config)

	parseStrategy := parseStrategy{}
	parser, err := parseStrategy.getParser(config)
	if err != nil {
		return nil, err
	}

	config, err = parser.ConfigParse()
	if err != nil {
		return nil, err
	}

	config.DBType = os.ExpandEnv(config.DBType)
	config.DSN = os.ExpandEnv(config.DSN)

	// Automatically parse postgres urls
	if config.DBType == postgresDriver {
		// Assumption: If we can parse the URL, we should
		if parsedURL, err := pq.ParseURL(config.DSN); err == nil && parsedURL != "" {
			config.DSN = parsedURL
		}
	}

	d := NewDBDriver(config.DBType, config.DSN)

	// allow the configuration to override the Import for this driver
	if config.Import != "" {
		d.Import = config.Import
	}

	// allow the configuration to override the Dialect for this driver
	if config.Dialect != "" {
		d.Dialect = dialectByName(config.Dialect)
	}

	if !d.IsValid() {
		return nil, errors.New(fmt.Sprintf("Invalid DBConf: %v", d))
	}

	// Extract to the end of the method
	return &DBConf{
		MigrationsDir: config.MigrationsPath,
		Env:           config.Env,
		Driver:        d,
		PgSchema:      config.PostgresSchema,
	}, nil
}

func setConfigDefaults(config *Config) {
	setDefaultString(&config.MigrationsPath, defaultDBMigrationsPath)
	setDefaultString(&config.DBConfigFilePath, defaultDBConfFilePath)
	setDefaultString(&config.DBTypeConfigKey, driverParameter)
	setDefaultString(&config.DSNConfigKey, openParameter)
	setDefaultString(&config.ImportConfigKey, importParameter)
	setDefaultString(&config.DialectConfigKey, dialectParameter)

	if isDir, err := isDirectory(config.DBConfigFilePath); err != nil {
		config.DBConfigFilePath = path.Join(defaultDBConfFilePath, defaultDBConfFile)
	} else if isDir {
		config.DBConfigFilePath = path.Join(config.DBConfigFilePath, defaultDBConfFile)
	}
}

func yamlKey(env, key string) string {
	return fmt.Sprintf(yamlKeyFormat, env, key)
}

func isDirectory(path string) (bool, error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false, err
	}

	return fileInfo.IsDir(), nil
}

// NewDBDriver create a new DBDriver and populate driver specific
// fields for drivers that we know about.
// Further customization may be done in NewDBConf
func NewDBDriver(name, open string) DBDriver {
	d := GetDBDriverProperties(name)
	d.OpenStr = open

	return d
}

func GetDBDriverProperties(driverName string) DBDriver {
	if driver, ok := dbDrivers[driverName]; ok {
		return driver
	}

	return DBDriver{Name: driverName}
}

// IsValid ensure we have enough info about this driver
func (drv *DBDriver) IsValid() bool {
	return len(drv.Import) > 0 && drv.Dialect != nil
}

// OpenDBFromDBConf wraps database/sql.DB.Open() and configures
// the newly opened DB based on the given DBConf.
//
// Callers must Close() the returned DB.
func OpenDBFromDBConf(conf *DBConf) (*sql.DB, error) {
	db, err := sql.Open(conf.Driver.Name, conf.Driver.OpenStr)
	if err != nil {
		return nil, err
	}

	// if a postgres schema has been specified, apply it
	if conf.Driver.Name == "postgres" && conf.PgSchema != "" {
		if _, err := db.Exec("SET search_path TO " + conf.PgSchema); err != nil {
			return nil, err
		}
	}

	return db, nil
}
