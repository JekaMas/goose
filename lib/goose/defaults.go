package goose

func setDefaultString(key *string, value string) {
	if *key == "" {
		*key = value
	}
}
