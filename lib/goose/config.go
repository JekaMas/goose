package goose

type Config struct {
	Env            string
	PostgresSchema string

	MigrationsPath   string
	DSN              string
	DBType           string
	DBConfigFilePath string
	Import           string
	Dialect          string

	DBTypeConfigKey  string
	DSNConfigKey     string
	ImportConfigKey  string
	DialectConfigKey string
}
