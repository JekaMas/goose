package goose

import (
	"errors"
	"fmt"
	"path/filepath"
)

type parseStrategy struct{}

type ConfigParser interface {
	ConfigParse() (Config, error)
}

func (this parseStrategy) getParser(config Config) (ConfigParser, error) {
	extension := filepath.Ext(config.DBConfigFilePath)[1:]

	switch extension {
	case iniExtension:
		return newIniParser(config), nil
	case yamlExtension:
		return newYamlParser(config), nil
	default:
		return nil, errors.New(fmt.Sprintf("Unknowm config format %q", extension))
	}
}
