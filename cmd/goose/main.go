package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"text/template"

	"bitbucket.org/JekaMas/goose/lib/goose"
)

// global options. available to any subcommands .
var (
	flagDBConfFile = flag.String("dbconf", "", "db configuration file")
	flagEnv        = flag.String("env", "", "which DB environment to use")
	flagPgSchema   = flag.String("pgschema", "", "which postgres-schema to migrate (default = none)")

	flagMigrationsPath = flag.String("migrations", "", "folder containing db migrations")
	flagDSN            = flag.String("dsn", "", "db dsn")
	flagDBType         = flag.String("dbtype", "", "db type: mysql, postgres, sqlite3, mymysql")

	flagDBTypeConfigKey  = flag.String("dbtypekey", "", "Key in config file for DB type")
	flagDSNConfigKey     = flag.String("dsnkey", "", "Key in config file for DSN write DB")
	flagImportConfigKey  = flag.String("importkey", "", "Key in config file for DB import package")
	flagDialectConfigKey = flag.String("dialectkey", "", "Key in config file for DB dialect")
)

func newConfigFromFlags() goose.Config {
	return goose.Config{
		Env:              *flagEnv,
		PostgresSchema:   *flagPgSchema,
		MigrationsPath:   *flagMigrationsPath,
		DSN:              *flagDSN,
		DBType:           *flagDBType,
		DBConfigFilePath: *flagDBConfFile,
		DBTypeConfigKey:  *flagDBTypeConfigKey,
		DSNConfigKey:     *flagDSNConfigKey,
		ImportConfigKey:  *flagImportConfigKey,
		DialectConfigKey: *flagDialectConfigKey,
	}
}

// helper to create a DBConf from the given flags
func dbConfFromFlags() (*goose.DBConf, error) {
	config := newConfigFromFlags()

	if config.DBConfigFilePath != "" {
		dbConfig, err := goose.NewDBConf(config)
		return dbConfig, err
	}

	dbConf := &goose.DBConf{}
	if config.PostgresSchema != "" {
		dbConf.PgSchema = config.PostgresSchema
	}
	if config.MigrationsPath != "" {
		dbConf.MigrationsDir = config.MigrationsPath
	}

	dbConf.Driver = goose.NewDBDriver(config.DBType, config.DSN)

	return dbConf, nil
}

var commands = []*Command{
	upCmd,
	downCmd,
	redoCmd,
	statusCmd,
	createCmd,
	dbVersionCmd,
}

func main() {

	flag.Usage = usage
	flag.Parse()

	args := flag.Args()
	if len(args) == 0 || args[0] == "-h" {
		flag.Usage()
		return
	}

	var cmd *Command
	name := args[0]
	for _, c := range commands {
		if strings.HasPrefix(c.Name, name) {
			cmd = c
			break
		}
	}

	if cmd == nil {
		fmt.Printf("error: unknown command %q\n", name)
		flag.Usage()
		os.Exit(1)
	}
	cmd.Exec(args[1:])
}

func usage() {
	fmt.Print(usagePrefix)
	flag.PrintDefaults()
	usageTmpl.Execute(os.Stdout, commands)
}

var usagePrefix = `
goose is a database migration management system for Go projects.

Usage:
    goose [options] <subcommand> [subcommand options]

Options:
`
var usageTmpl = template.Must(template.New("usage").Parse(
	`
Commands:{{range .}}
    {{.Name | printf "%-10s"}} {{.Summary}}{{end}}
`))
